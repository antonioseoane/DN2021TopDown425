@echo off
setlocal


SET UE_PROJECT=
for %%f in (*.uproject) do (
	SET UE_PROJECT=%%f
)

@echo Unreal Project: ^<%UE_PROJECT%^>

for /f "tokens=1,2 delims=:, " %%a in (' find "EngineAssociation" ^< %UE_PROJECT% ') do (
    set UE_REQUIRED_VERSION=%%~b
)

@echo Unreal Project Version: %UE_REQUIRED_VERSION%

@rem set UE4_425_PATH = D:\UE4\UE_4.25
@rem FOR /F "tokens=*" %%g IN ('powershell -command "& { (Get-ItemProperty 'Registry::HKEY_LOCAL_MACHINE\SOFTWARE\EpicGames\Unreal Engine\4.26' -Name 'InstalledDirectory' ).'InstalledDirectory' }"') do (SET UE4_PATH="%%g")

set KEY_NAME="HKEY_LOCAL_MACHINE\SOFTWARE\EpicGames\Unreal Engine\%UE_REQUIRED_VERSION%"
set VALUE_NAME=InstalledDirectory
if not defined UE4_PATH (
    for /f "usebackq tokens=1,2,*" %%A in (`reg query %KEY_NAME%  /reg:64`) do (
        if "%%A" == "%VALUE_NAME%" (
            set UE_PATH=%%C
        )
    )
    if not defined UE_PATH goto error_unreal_no_found
)

echo Unreal Engine installed at: ^<%UE_PATH%^>


@rem BUILD PROJECT
@rem %UE4_425_PATH%\Engine\Build\BatchFiles\RunUAT BuildCookRun -nocompileeditor -installed -nop4 -project=%CD%/%UE4_PROJECT% -cook -stage -archive -archivedirectory=%CD%/Build -package -pak -prereqs -nodebuginfo -targetplatform=Win64 -clientconfig=Development -utf8output
"%UE_PATH%"\Engine\Build\BatchFiles\RunUAT BuildCookRun -nocompileeditor -installed -nop4 -project="%CD%/%UE_PROJECT%" -cook -stage -archive -archivedirectory="%CD%/Build" -package -clean -compressed -SkipCookingEditorContent -pak -prereqs -distribution -nodebuginfo -targetplatform=Win64 -build -clientconfig=Shipping -utf8output


goto :eof


:error_unreal_no_found
echo Unreal Engine %UE_REQUIRED_VERSION% not found!
goto :eof

