@echo off
if exist "%CD%\Binaries" rmdir /S/Q "%CD%\Binaries"

if exist "%CD%\Build\Windows\Application.ico" move "%CD%\Build\Windows\Application.ico" "%CD%"
if exist "%CD%\Build" rmdir /S/Q "%CD%\Build"
if exist "%CD%\Application.ico" mkdir "%CD%\Build" & mkdir "%CD%\Build\Windows" & move "%CD%\Application.ico" "%CD%\Build\Windows"

if exist "%CD%\DerivedDataCache" rmdir /S/Q "%CD%\DerivedDataCache
if exist "%CD%\Intermediate" rmdir /S/Q "%CD%\Intermediate"
if exist "%CD%\Saved" rmdir /S/Q "%CD%\Saved"